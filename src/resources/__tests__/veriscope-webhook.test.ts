import { describe, beforeAll, it, expect } from 'vitest';
import VeriscopeWebhook, {
  VeriscopeWebhookEventType,
  VeriscopeWebhookPayload,
  VeriscopeWebhookPayload_NEW_ATTESTATION,
} from '../veriscope-webhook';

describe('VeriscopeWebhook', () => {
  let veriscopeWebhook: VeriscopeWebhook;

  beforeAll(() => {
    veriscopeWebhook = new VeriscopeWebhook({
      veriscopeNodeURL: '',
      veriscopeNodeToken: '',
      veriscopeWebHookSecret: 'mi_secreto',
    });
  });

  it.skip('should sign a payload correctly', () => {
    const payload = {
      AttestationHash:
        '0x1f297bf758f83828f4145643a06a23e4307d4b63cba885ac52c96fcd362fc29d',
      BeneficiaryTAAddress: '0xC0cA43B4848823d5417cAAFB9e8E6704b9d5375c',
      BeneficiaryTAPublicKey:
        '0x04c2d213d585fed2213395e61f99b988f692aec84d38b288d14ffc4a90c879531cd92105bf1336da88ec6bf1f86a74293695cfeaa444dc9fcf2614d201e4a64c02',
      BeneficiaryUserAddress: '0xb532cca105f966a76c3826451818b55fb2190933',
      BeneficiaryUserPublicKey:
        '0x04030d33064a0312133b5c658d6639776c2583f536d683d337dcbef9a7a92b3e948309ed6d539af0be4789f2cb12a7f307b5f3b2bba5691d38b7f22780c7f9cf06',
      BeneficiaryTASignatureHash:
        '0x0b709dd4809f36a22fe48250b24a5e41e8aea491bace26627f5c68ea9b4fad3f',
      BeneficiaryTASignature: {
        r: '0x46f872cf316d3dfec32647408cb368fb2d03e99bbd8a96dd6f98548d5ab1e9ab',
        s: '0x6a2eda3f0fc77a0753d983709a851bb2e66e0d1aa13515a008658f24f252206f',
        v: '0x25',
      },
      BeneficiaryUserSignatureHash:
        '0x7ec005c40fadb64f4180dcc14d9f5927f649096a08478a4a5a112a3aa77ca549',
      BeneficiaryUserSignature: {
        r: '0x44b6fd5ca7bd65df4b63e532783ab9fba32021677bc86d25a19901b2bcc25212',
        s: '0x2e005a04016ffb0a4be91feb00a91870fe5b82141f6d901d0f949143c05146c2',
        v: '0x25',
      },
      BeneficiaryUserAddressCryptoProof: {
        chain: 'BTC-mainnet',
        asset: 'BTC',
        address_type: 'P2PKH',
        address: '18crLganfufUzqFB2eH7jt4jPPYze45gZs',
        trust_anchor_pubkey:
          '0x04c2d213d585fed2213395e61f99b988f692aec84d38b288d14ffc4a90c879531cd92105bf1336da88ec6bf1f86a74293695cfeaa444dc9fcf2614d201e4a64c02',
        pubkey:
          '02bc52ddcc4f24f7c180a2a7aba994a9b58ebd2f91e3b061a82734a86709cd936d',
        signature:
          'H6WiQvsUan2Rn81848TSC0xs8sTzGbZDAhgcm0cvBhbxYTzshFV6BKFBGOJgDo33R+5cYRb7+8HQbSklBaRiDWs=',
      },
      BeneficiaryUserAddressCryptoProofStatus: true,
      CoinBlockchain: 'tBTC',
      CoinToken: 'tBTC',
      CoinAddress: '18crLganfufUzqFB2eH7jt4jPPYze45gZs',
      CoinMemo:
        'notabene:veriscope:addressDiscovery:eb92b71e-2990-41fe-acb3-c441d0017588',
      CoinTransactionHash: null,
      CoinTransactionValue: null,
      SenderTAAddress: '0x9fb4f3795f95eefa8feb1161ad7dd9bce94d5ccf',
      SenderTAPublicKey: null,
      SenderUserAddress: '0xf6f21d527dafa99e25c2a4bf4be85ad6c59cd21e',
      SenderUserPublicKey: null,
      SenderTASignatureHash: null,
      SenderTASignature: null,
      SenderUserSignatureHash: null,
      SenderUserSignature: null,
      BeneficiaryKYC: null,
      SenderKYC: null,
      BeneficiaryTAUrl: 'https://pcf.veriscope.network/kyc-template',
      SenderTAUrl: 'https://notabene.veriscope.org/kyc-template',
    };
    const signature = veriscopeWebhook.signPayload(payload);
    expect(signature).toEqual(
      'e68dbeceda32fd9c5e3efa4d37911c63e6d9589f98f79b10be2c11d7ade8095f'
    );
  });

  it('should handle unknown eventType', async () => {
    expect.assertions(1);
    try {
      await veriscopeWebhook.handleWebhookPayload({
        eventType: 'UNKNOWN',
      } as any);
    } catch (e) {
      expect(e.message).toMatch('no handler for webhook eventType: UNKNOWN');
    }
  });

  it('should handle a NEW_ATTESTATION', async () => {
    const handler = async (payload: VeriscopeWebhookPayload): Promise<void> => {
      expect(payload.eventType).to.eq(
        VeriscopeWebhookEventType.NEW_ATTESTATION
      );
    };

    veriscopeWebhook.registerEventTypeHandler(
      VeriscopeWebhookEventType.NEW_ATTESTATION,
      handler
    );

    const payload: VeriscopeWebhookPayload_NEW_ATTESTATION = {
      eventType: VeriscopeWebhookEventType.NEW_ATTESTATION,
      ta_account: '0xa8E32fbA1FaE65e010F9e5AbA32cBa37243bbc48',
      jurisdiction: '196',
      effective_time: '1600358306',
      expiry_time: '1663603106',
      is_managed: '1',
      attestation_hash:
        '0x6685d80f13a8f590dbbda95409bad359d903d8d373283e7be6dfed5b50da0a73',
      transaction_hash:
        '0x46d736857ea589dc5a087b4a07bb3d256fbbff4bef450fa2e5c1ddbcd737b7b4',
      user_account: '0xf6EA8e0D3072F93f7f945148a4A27AB4f14c6a9d',
      public_data: '0x57414c4c4554',
      public_data_decoded: 'WALLET',
      documents_matrix_encrypted:
        '0x373839633835346534313061383334303063313435663532373239653433623265656261626231656662386435326138353033643462623138373532306162656330333739626538623165303065313932366363363432303535373538616362373239653937353031376632343732313566626633343766613638313361626162656337373137383131653864396366336437353337396632316563323164323238353539643533313532346465343539636165353132663863636436313331323362336239343931303363353264653862326164316561643630623038336139616632363162303939306539316431303662346631666566623766373030336263613632303639',
      documents_matrix_encrypted_decoded:
        '789c854e410a83400c145f52729e43b2eebabb1efb8d52a8503d4bb187520abec0379be8b1e00e1926cc642055758acb729e975017f247215fbf347fa6813ababec7717811e8d9cf3d75379f21ec21d228559d531524de459cae512f8ccd613123b3b949103c52de8b2ad1ead60b083a9af261b0990e91d106b4f1fefb7f7003bca62069',
      availability_address_encrypted:
        '0x2020202020202020202020202020202020202020202020202020202020425443',
      availability_address_encrypted_decoded: 'BTC',
      version_code: '2',
      coin_blockchain: 'BTC',
      coin_token: 'BTC',
      coin_address: '1hqrzoPoyWkgodZWQ6YrYJxs732xdpFAC',
      coin_memo: 'WALLET',
    };
    await veriscopeWebhook.handleWebhookPayload(payload);
  });
});
