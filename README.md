<div align="center">

<img src="https://uploads-ssl.webflow.com/63a23376dc2eb1ed6d789406/63a23376dc2eb1fe8278951a_veriscope-logo-v3-white.svg" height=50>

<br>

# Shyft Veriscope NodeJS SDK

[![pipeline status](https://gitlab.com/notabene/open-source/code-travel-rule-nodejs-sdk/-/badges/pipeline.svg)](https://gitlab.com/notabene/open-source/code-travel-rule-nodejs-sdk/-/commits/master)
[![Latest Release](https://gitlab.com/notabene/open-source/code-travel-rule-nodejs-sdk/-/badges/release.svg)](https://gitlab.com/notabene/open-source/code-travel-rule-nodejs-sdk/-/releases)

NodeJS compatible library to connect to Shyft Veriscope Travel Rule platform

[Getting started](#getting-started) •
[Installation](#installation) •
[Configuration](#configuration)

</div>

## Getting Started

### Step 1: Install the library

```
npm install @notabene/veriscope
```

### Step 2: Initialize the client

```javascript
const { Veriscope } = require('@notabene/veriscope');

const veriscope = new Veriscope({
  veriscopeNodeURL: 'http://localhost', //Add your own Veriscope Node URL
  veriscopeNodeToken: 'eyASdsd......', //Use your own API token
  veriscopeWebHookSecret: 'secret', //Use your own secret
});
```

## Configuration

?

### Veriscope Node URL, Node Token and Webhook Secret

All this values come from Veriscope Node.

## API methods

- `veriscope.api.createShyftUser()` creates a Shyft user
- `veriscope.api.setV3Attestation(payload)` sends an Attestation to Veriscope
- `veriscope.api.createKYCtemplate(payload)` creates or updates an KYC template
- `veriscope.api.encryptIVMS()` Encrypts KYC/IVMS data with counterparty public key
- `veriscope.api.decryptIVMS()` Decrypts KYC/IVMS data with counterparty public key
- `veriscope.api.getTrustAnchorAccounts()` get configured TrustAnchor accounts in the node
- `veriscope.api.getTrustAnchorDetails()` get TrustAnchor details fields
- `veriscope.api.getJurisdictions()` get jurisdictions
- `veriscope.api.getVerifiedTrustAnchors()` get array of verified trust anchors

## Appendix

### Example 1

## [License](LICENSE.md)

BSD 3-Clause © Notabene Inc.
