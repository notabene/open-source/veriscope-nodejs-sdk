import { AxiosInstance } from 'axios';
import querystring from 'querystring';

export default class VeriscopeAPI {
  private http: AxiosInstance;

  constructor(http: AxiosInstance) {
    this.http = http;
  }

  /**
   * Create Shyft User
   *
   * Creates a Shyft user
   * @returns {ShyftUser}
   */
  public async createShyftUser(): Promise<ShyftUser> {
    return await this.http.post('/create_shyft_user');
  }

  /**
   * Set V3 Attestation
   *
   *
   * @returns []
   */
  public async setV3Attestation(payload: SetV3AttestationPayload): Promise<[]> {
    return await this.http.post('/set_v3_attestation', payload);
  }

  /**
   * Create KYC Template
   *
   * Creates or updates an KYC template
   * @returns {KYCtemplate}
   */
  public async createKYCtemplate(
    payload: CreateKYCtemplatePayload
  ): Promise<KYCtemplate> {
    const body = JSON.parse(JSON.stringify(payload));
    body.user_signature = JSON.stringify(payload.user_signature);
    return await this.http.post(
      '/create_kyc_template',
      querystring.stringify(body),
      {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      }
    );
  }

  /**
   * Encrypt IVMS
   *
   * Encrypts KYC/IVMS data with counterparty public key
   *
   * @returns {EncryptedIVMS}
   */
  public async encryptIVMS(
    payload: EncryptIVMSPayload
  ): Promise<EncryptedIVMS> {
    return await this.http.post('/encrypt_ivms', payload);
  }

  /**
   * Decrypt IVMS
   *
   * Decrypt KYC/IVMS data with private key
   *
   * @returns {DecryptedIVMS}
   */
  public async decryptIVMS(
    payload: DecryptIVMSPayload
  ): Promise<DecryptedIVMS> {
    return await this.http.post('/decrypt_ivms', payload);
  }

  /**
   * Get Trust Anchor Accounts
   * Gets the TA (VASPs) accounts handled by the Veriscope node
   * @returns {TrustAnchorAccount[]}
   */
  public async getTrustAnchorAccounts(): Promise<TrustAnchorAccount[]> {
    return await this.http.get('/get_trust_anchor_accounts');
  }

  /**
   * Get Trust Anchors Details
   * @param ta_account
   * @returns {TrustAnchorDetailField[]}
   */
  public async getTrustAnchorDetails(
    ta_account: string
  ): Promise<TrustAnchorDetailField[]> {
    return await this.http.get('/get_trust_anchor_details/' + ta_account);
  }

  /**
   * Get Jurisdictions
   * @param ta_account
   * @returns {Jurisdiction[]}
   */
  public async getJurisdictions(): Promise<Jurisdiction[]> {
    return await this.http.get('/get_jurisdictions');
  }

  /**
   * Get Verified Trust Anchors
   * @returns {VerifiedTrustAnchor[]}
   */
  public async getVerifiedTrustAnchors(): Promise<VerifiedTrustAnchor[]> {
    return await this.http.get('/get_verified_trust_anchors');
  }
}

export type ShyftUser = {
  account_address: string;
  private_key: string;
  public_key: string;
  message: string;
  signature_hash: string;
  signature: {
    r: string;
    s: string;
    v: string;
  };
};

export type SetV3AttestationPayload = {
  ta_account: string;
  user_account: string;
  jurisdiction: string;
  coin_blockchain: string;
  coin_token: string;
  coin_address: string;
  coin_memo: string;
};

export type CreateKYCtemplatePayload = {
  ta_account: string; //Use TA account address
  attestation_hash: string; //Attestation Hash from attestation
  user_account: string; //Use account_address from Create Shyft User
  user_public_key: string; //Use public_key from Create Shyft User
  user_signature: {
    // Use signature from Create Shyft User
    r: string;
    s: string;
    v: string;
  };
  user_signature_hash: string; //Use signature_hash from Create Shyft User
  ivms_encrypt?: string; //Use data generated from Encrypt IVMS
  coin_transaction_hash?: string; //Coin Transaction Hash from crypto transaction (optional)
  coin_transaction_value?: string; //Coin Transaction Value from crypto transaction (optional)
  ivms_state_code?: string; //Accept or reject VASPs request
  coin_address_crypto_proof?: {
    //Coin address crypto proof for the oVASP to validate
    chain: string;
    asset: string;
    address_type: string;
    address: string;
    trust_anchor_pubkey: string;
    pubkey: string;
    signature: string;
  };
};

export type KYCtemplate = {
  id: number;
  attestation_hash: string;
  beneficiary_ta_address: string;
  beneficiary_ta_public_key: string;
  beneficiary_user_address: string;
  beneficiary_user_public_key: string;
  beneficiary_ta_signature_hash: string;
  beneficiary_ta_signature: string;
  crypto_address_type: string;
  crypto_address: string;
  crypto_public_key: string;
  crypto_signature_hash: string;
  crypto_signature: string;
  sender_ta_address: string;
  sender_ta_public_key: string;
  sender_user_address: string;
  sender_user_public_key: string;
  sender_ta_signature_hash: string;
  sender_ta_signature: string;
  payload: string;
  beneficiary_kyc: string;
  sender_kyc: string;
  created_at: string;
  updated_at: string;
  kyc_template_state_id: string;
  beneficiary_user_signature_hash: string;
  beneficiary_user_signature: string;
  sender_user_signature_hash: string;
  sender_user_signature: string;
  beneficiary_ta_url: string;
  sender_ta_url: string;
  beneficiary_kyc_decrypt: string;
  sender_kyc_decrypt: string;
  coin_blockchain: string;
  coin_token: string;
  coin_address: string;
  coin_memo: string;
  coin_transaction_hash: string;
  coin_transaction_value: string;
  status: string;
  webhook_status: string;
  ivms_status: string;
  test_vars: string;
  beneficiary_user_address_crypto_proof: string;
  beneficiary_user_address_crypto_proof_status: boolean;
  system_ta_account: string;
  owner: string;
};

export type EncryptIVMSPayload = {
  public_key: string;
  ivms_json: string;
};

export type EncryptedIVMS = {
  data: string;
};

export type DecryptIVMSPayload = {
  private_key: string;
  kyc_data: string;
};

export type DecryptedIVMS = {
  data: string;
};

export type TrustAnchorAccount = {
  id: number;
  ta_prefname: string;
  ta_jurisdiction: number;
  user_id: number;
  created_at: string;
  updated_at: string;
  account_address: string;
  public_key: string;
  signature_hash: string;
  signature: string;
  /*
  legal_person_name: null;
  legal_person_name_identifier_type: null;
  address_type: null;
  street_name: null;
  building_number: null;
  building_name: null;
  postcode: null;
  town_name: null;
  country_sub_division: null;
  country: null;
  department: null;
  sub_department: null;
  floor: null;
  room: null;
  town_location_name: null;
  district_name: null;
  address_line: null;
  postbox: null;
  customer_identification: null;
  national_identifier: null;
  national_identifier_type: null;
  country_of_registration: null;
  */
};

export type TrustAnchorDetailField = {
  id: number;
  transaction_hash: string;
  trust_anchor_address: string;
  key_value_pair_name: string;
  key_value_pair_value: string;
  created_at: string;
  updated_at: string;
  block_number: number;
};

export type Jurisdiction = {
  id: number;
  sortname: string;
  name: string;
  created_at: string;
  updated_at: string;
};

export type VerifiedTrustAnchor = {
  id: number;
  account_address: string;
  created_at: string;
  updated_at: string;
  block_number: number;
};
