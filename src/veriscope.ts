import { AxiosInstance } from 'axios';
import { createClient } from './utils/create-client';
import VeriscopeAPI from './resources/veriscope-api';
import VeriscopeWebhook from './resources/veriscope-webhook';

export type VeriscopeConfig = {
  veriscopeNodeURL: string;
  veriscopeNodeToken: string;
  veriscopeWebHookSecret?: string;

  userAgentHeader?: string;
};

const defaults: Partial<VeriscopeConfig> = {
  userAgentHeader: 'notabene/veriscope',
};

export class Veriscope {
  public http: AxiosInstance;
  public api: VeriscopeAPI;
  public webhook: VeriscopeWebhook;

  constructor(config: VeriscopeConfig) {
    const configWithDefaults = { ...defaults, ...config };
    this.http = createClient(configWithDefaults);

    this.api = new VeriscopeAPI(this.http);
    this.webhook = new VeriscopeWebhook(configWithDefaults);
  }
}
