import {
  CreateKYCtemplatePayload,
  DecryptIVMSPayload,
  EncryptIVMSPayload,
  ShyftUser,
} from 'src/resources/veriscope-api';
import { describe, beforeAll, it, expect } from 'vitest';
import { Veriscope } from '..';

describe.skip('Veriscope', () => {
  let veriscope: Veriscope;
  const trustAnchor = '0x9fB4f3795F95EEfa8FEB1161ad7DD9bCE94d5ccf';
  let shyftUser: ShyftUser;

  beforeAll(() => {
    veriscope = new Veriscope({
      veriscopeNodeURL: 'https://notabene.veriscope.org',
      veriscopeNodeToken:
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwiaXNzIjoic2VsZiIsImp0aSI6ImVmMGY3MWQzYjIyNTEyNGVjMTZhZDVjOTI5NGVmZTE0ODhlMzZkMTE4MmUzYzlkYTg0YmQ1NDQxZmU0MzVlMTA2NjlkYWM1Y2YzNjg1M2EyIiwiaWF0IjoxNjgzNTEyMTg0LjY0NjkyMywibmJmIjoxNjgzNTEyMTg0LjY0NjkyMywiZXhwIjoxNzEzNjg3OTQxLCJzdWIiOiIxIiwic2NvcGVzIjpbInNldF9hdHRlc3RhdGlvbiIsImNyZWF0ZV9zaHlmdF91c2VyIiwiZ2V0X2p1cmlzZGljdGlvbnMiLCJnZXRfdmVyaWZpZWRfdHJ1c3RfYW5jaG9ycyIsImdldF90cnVzdF9hbmNob3JfZGV0YWlscyIsInZlcmlmeV90cnVzdF9hbmNob3IiXX0.N1R_goGTCfnF2dUr_c6NEizgQadqjQe0gsKuhucWx_KsUc4gw_hyyf_PJNCCzyts-UuXJditwG3ShvdbpZ6dfpumnZjkQYDw8Vo5QBkS24DF45dQFw2GOOa1hbOkoIMtrAsr9eUaWgFdu8R2b9u2iXcyB4hnfTRojnaZ7K5uNVJOxAObeU1NSkY9Ip6MecZ0Nfy50y9eERuQzqAM37nYNG3Gh_V-e28lnb_xwx-cIUeJOVik0roHkAHR2QPVoIPaikQD_29umu0Bwqb-GKfE-TbupF06SizeNj4OJea023opnGnZ4X_a84NeKo4JmN-L7rb3F-Fd0EV097pjAAuLgzsz6qSAzgQiPR0LHSPDnQE_JlOhG5D8X02GuePDukHHEU6i2ig9AEwiVvbvQ8NOqCWT7Ajs6CTuUotCeTXVeLrO70exY7c5N8qwdR_K0ed2UwhDqq_ZEeFoUjQ499NW9GJAq1pJelZakgsgfApaBsg86nYAErtfKvDiISkEzZP72ACOyLiP51_2FKQO7yJlvRkq6764zCfMCsVLOVnpThcEDtAwaJtxJTprDL1_NB529U7o-KbMW8jw1HIPv44Gs9kVrrv2dBFBd3hWvjiqjY_WwlsM0ep27UizO2WMfmYu1_4PbSbWfKd6O-NPn0q5vEh9Dzu5RfF13GiPaAk5TBY',
      veriscopeWebHookSecret: 'mi_secreto',
    });
  });

  it('should create a shyft user', async () => {
    const resp = await veriscope.api.createShyftUser();
    expect(resp).to.be.a('object');
    expect(resp.account_address).to.be.a('string');
    shyftUser = resp;
  });

  it.skip('should set an attestation', async () => {
    if (!shyftUser) {
      shyftUser = await veriscope.api.createShyftUser();
    }
    const r = Date.now();
    const payload = {
      ta_account: trustAnchor,
      user_account: shyftUser.account_address,
      jurisdiction: '69',
      coin_blockchain: 'tBTC',
      coin_token: 'tBTC',
      coin_address: '18crLganfufUzqFB2eH7jt4jPPYze45gZs',
      coin_memo: JSON.stringify({ testMessage: 'testing-' + r }),
    };
    const resp = await veriscope.api.setV3Attestation(payload);
    expect(resp).to.be.a('array');
    expect(resp.length).to.eq(0);
  });

  it.skip('should create a kyc template', async () => {
    if (!shyftUser) {
      shyftUser = await veriscope.api.createShyftUser();
    }
    const payload: CreateKYCtemplatePayload = {
      ta_account: trustAnchor,
      attestation_hash:
        '0x02afaeb9305b9eda99da6d2da6d68e3f1243dd8100a5ddba197b2c6f5bb85100',
      user_account: shyftUser.account_address,
      user_public_key: shyftUser.public_key,
      user_signature: shyftUser.signature,
      user_signature_hash: shyftUser.signature_hash,
    };
    const resp = await veriscope.api.createKYCtemplate(payload);
    expect(resp.attestation_hash).to.eq(payload.attestation_hash);
    expect(resp.sender_ta_address).to.eq(payload.ta_account);
  });

  it('should encrypt and decrypt an IVMS structure', async () => {
    const user = await veriscope.api.createShyftUser();
    const ivmsObjStr = JSON.stringify({
      orignator: {},
      beneficiary: {},
    });
    const payload: EncryptIVMSPayload = {
      public_key: user.public_key,
      ivms_json: ivmsObjStr,
    };
    const resp = await veriscope.api.encryptIVMS(payload);
    expect(resp).to.be.a('Object');
    expect(resp.data).to.be.a('string');

    const payload2: DecryptIVMSPayload = {
      private_key: user.private_key,
      kyc_data: resp.data,
    };
    const resp2 = await veriscope.api.decryptIVMS(payload2);
    console.log(resp2);
    expect(resp2.data).to.be.a('string');
    expect(resp2.data).to.eq(ivmsObjStr);
  });

  it('should get the trust anchor accounts', async () => {
    const resp = await veriscope.api.getTrustAnchorAccounts();
    expect(resp).to.be.a('array');
    expect(resp.length).to.be.gt(0);
  });

  it('should get the trust anchor details', async () => {
    const resp = await veriscope.api.getTrustAnchorDetails(trustAnchor);
    expect(resp).to.be.a('array');
    expect(resp.length).to.be.gt(0);
    expect(resp[0].trust_anchor_address).to.eq(trustAnchor);
  });

  it('should get the jurisdictions', async () => {
    const resp = await veriscope.api.getJurisdictions();
    expect(resp).to.be.a('array');
    expect(resp.length).to.be.gt(100);
  });

  it('should get the verified trust anchors', async () => {
    const resp = await veriscope.api.getVerifiedTrustAnchors();
    expect(resp).to.be.a('array');
    expect(resp.length).to.be.gt(1);
    //Our trustAnchor should be in the list
    const foundTa = resp.find((vta) => {
      return vta.account_address == trustAnchor;
    });
    expect(foundTa).to.not.be.null;
    expect(foundTa?.account_address).to.eq(trustAnchor);
  });
});
