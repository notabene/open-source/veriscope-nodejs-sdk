import CryptoJS from 'crypto-js';
import { VeriscopeConfig } from '../veriscope';

export default class VeriscopeWebhook {
  private webhookSecret: string | undefined;
  private eventTypeHandlers: {
    [func: string]: (payload: VeriscopeWebhookPayload) => Promise<void>;
  };

  constructor(config: VeriscopeConfig) {
    this.webhookSecret = config.veriscopeWebHookSecret;
    this.eventTypeHandlers = [] as any;
  }

  /**
   * Sign Webhook payload. To check if its valid
   * See: https://docs.veriscope.network/docs/latest/learn/webhook/webhook-config-for-horizon.html#signed-signature-verification
   *
   * @returns signature
   */
  public signPayload(payload: any): string {
    if (this.webhookSecret === undefined) {
      throw Error('webhook secret not defined');
    }

    return CryptoJS.HmacSHA256(payload, this.webhookSecret).toString(
      CryptoJS.enc.Hex
    );
  }

  /**
   * Register a handler for the eventType
   *
   * @param eventType
   * @param handler
   */
  public registerEventTypeHandler(
    eventType: VeriscopeWebhookEventType,
    handler: (payload: VeriscopeWebhookPayload) => Promise<void>
  ) {
    this.eventTypeHandlers[eventType] = handler;
  }

  /**
   * Handle webhook payload
   *
   *
   * @returns
   */
  public async handleWebhookPayload(
    payload: VeriscopeWebhookPayload
  ): Promise<void> {
    if (this.eventTypeHandlers[payload.eventType]) {
      const handler = this.eventTypeHandlers[payload.eventType];
      await handler(payload);
    } else
      throw Error('no handler for webhook eventType: ' + payload.eventType);
  }
}

export type VeriscopeWebhookPayload =
  | VeriscopeWebhookPayload_NEW_ATTESTATION
  | VeriscopeWebhookPayload_OR_DATA_REQ;

export enum VeriscopeWebhookEventType {
  NEW_ATTESTATION = 'NEW_ATTESTATION',
  OR_DATA_REQ = 'OR_DATA_REQ',
}

export type VeriscopeWebhookPayload_NEW_ATTESTATION = {
  eventType: VeriscopeWebhookEventType;
  ta_account: string;
  jurisdiction: string;
  effective_time: string;
  expiry_time: string;
  is_managed: string;
  attestation_hash: string;
  transaction_hash: string;
  user_account: string;
  public_data: string;
  public_data_decoded: string;
  documents_matrix_encrypted: string;
  documents_matrix_encrypted_decoded: string;
  availability_address_encrypted: string;
  availability_address_encrypted_decoded: string;
  version_code: string;
  coin_blockchain: string;
  coin_token: string;
  coin_address: string;
  coin_memo: string;
};

export type VeriscopeWebhookPayload_OR_DATA_REQ = {
  eventType: VeriscopeWebhookEventType;
  kycTemplate: {
    AttestationHash: string;
    BeneficiaryTAAddress: string;
    BeneficiaryTAPublicKey: string;
    BeneficiaryUserAddress: string;
    BeneficiaryUserPublicKey: string;
    BeneficiaryTASignatureHash: string;
    BeneficiaryTASignature: {
      r: string;
      s: string;
      v: string;
    };
    BeneficiaryUserSignatureHash: string;
    BeneficiaryUserSignature: {
      r: string;
      s: string;
      v: string;
    };
    BeneficiaryUserAddressCryptoProof: {
      chain: string;
      asset: string;
      address_type: string;
      address: string;
      trust_anchor_pubkey: string;
      signature: string;
    };
    BeneficiaryUserAddressCryptoProofStatus: boolean;
    CoinBlockchain: string;
    CoinToken: string;
    CoinAddress: string;
    CoinMemo: string;
    CoinTransactionHash: string;
    CoinTransactionValue: string;
    SenderTAAddress: string;
    SenderTAPublicKey: string;
    SenderUserAddress: string;
    SenderUserPublicKey: string;
    SenderTASignatureHash: string;
    SenderTASignature: string;
    SenderUserSignatureHash: string;
    SenderUserSignature: string;
    BeneficiaryKYC: string;
    SenderKYC: string;
    BeneficiaryTAUrl: string;
    SenderTAUrl: string;
  };
};
