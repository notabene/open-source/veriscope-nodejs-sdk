import axios from 'axios';
import { VeriscopeConfig } from '../veriscope';
import freezeSys from './freeze-object';

const errorInterceptor = (error: any) => {
  return Promise.reject({
    req: {
      url: '' + error?.response?.config?.baseURL + error?.response?.config?.url,
      method: error?.request?.method,
    },
    status: error?.response?.status,
    statusText: error?.response?.statusText,
    err: JSON.stringify(
      error?.response?.data?.err || error?.response?.data || error
    ),
  });
};

axios.interceptors.request.use(
  (res) => res,
  (error) => errorInterceptor(error)
);
axios.interceptors.response.use(
  (res) => res,
  (error) => errorInterceptor(error)
);

export function createClient(config: VeriscopeConfig) {
  const client = axios.create({
    baseURL: config.veriscopeNodeURL + '/api/v1/server',
    headers: {
      Authorization: 'Bearer ' + config.veriscopeNodeToken,
      'Content-Type': 'application/json',
      'User-Agent': config.userAgentHeader,
    },
  });

  client.interceptors.request.use(
    (res) => res,
    (error) => errorInterceptor(error)
  );
  client.interceptors.response.use(
    (res) => res,
    (error) => errorInterceptor(error)
  );

  client.interceptors.response.use((response) => {
    return freezeSys(response.data);
  });

  return client;
}
